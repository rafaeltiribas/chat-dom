function excluirMensagem(mensagem){mensagem.parentNode.remove();}

function editaMensagem(mensagem){
}

function enviaMensagem(){
    //criar mensagem
    let mensagem = document.querySelector("#mensagem")
    let msg_box = document.querySelector(".section_mensages")
    let escopo = document.createElement("div");
    let texto = document.createElement("p");
    texto.innerText = mensagem.value;
    //criar botoes
    let btn_apaga = document.createElement("button");
    btn_apaga.innerText = "X";
    btn_apaga.classList.add("apagar");
    btn_apaga.setAttribute("onclick","excluirMensagem(this)");
    escopo.append(btn_apaga);

    let btn_edita = document.createElement("button");
    btn_edita.innerText = "Editar";
    btn_edita.classList.add("edita");
    escopo.append(btn_edita);


    //adiciona a pagina
    escopo.append(texto);
    msg_box.append(escopo);
}